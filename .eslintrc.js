module.exports = {
	'env': {
		'browser': true,
		'es2020': true,
		'node': true,
		'jest': true	//whitelist the environment variables provided by Jest by doing
	},
	'extends': 'eslint:recommended',
	'parserOptions': {
		'ecmaVersion': 11
	},
	'rules': {
		'quotes': [
            'error',
            'single'
        ],
        'semi': [
            'error',
            'always'
        ]
	}
};
