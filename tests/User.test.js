
var userList = [];
test('Create of user logic', () => {
    userList.push(1);
    expect(userList.length).toBe(1);
});

test('update of user logic', () => {
    userList[0] = 2;
    expect(userList[0]).toBe(2);
});

test('delete of user logic', () => {
    userList.pop();
    expect(userList).toStrictEqual([]);
});