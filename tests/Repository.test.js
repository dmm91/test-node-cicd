
var repositoryList = [];
test('Create of repository logic', () => {
    repositoryList.push(1);
    expect(repositoryList.length).toBe(1);
});

test('update of repository logic', () => {
    repositoryList[0] = 2;
    expect(repositoryList[0]).toBe(2);
});

test('delete of repository logic', () => {
    repositoryList.pop();
    expect(repositoryList).toStrictEqual([]);
});