var PORT = process.env.PORT || 5000;
var express = require('express');
var app = express();

var http = require('http');
var server = http.Server(app);

app.use(express.static('client'));

app.get('/', function (req, res) {
  res.send('This is the a request from home from the presentation');
  console.log('home');
  console.log('home tests');
});

app.get('/test', function (req, res) {
  res.send('This is a request from /test ');
  console.log('test endpoint');
});

server.listen(PORT, function() {
  console.log('Server is running on port: [' + PORT + ']');
});
